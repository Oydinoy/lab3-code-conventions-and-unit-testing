package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.models.*;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Thread;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class threadControllerTest {

    private Thread testThread;
    private List<Post> emptyArray;
    private Vote testVote;


    @BeforeEach
    @org.junit.jupiter.api.DisplayName("All for testing")
    void createStubs() {
        emptyArray = Collections.emptyList();

        testThread = new Thread(
                "Oydinoy",
                new Timestamp(2121348928345L),
                "forum",
                "message",
                "slug",
                "title",
                12
        );
    }

    @org.junit.jupiter.api.Test
    @org.junit.jupiter.api.DisplayName("Post creation test")
    void createPostTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(1))
                    .thenReturn(testThread);

            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                    .thenReturn(testThread);

            try (MockedStatic userDAO = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();

                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(emptyArray), controller.createPost("slug", emptyArray), "Creating by empty post");
            }
            assertEquals(testThread, ThreadDAO.getThreadBySlug("slug"));
        }
    }

    @org.junit.jupiter.api.Test
    @org.junit.jupiter.api.DisplayName("Getting non-real slug")
    void CheckIdOrSlugTest(){
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(1))
                    .thenReturn(testThread);

            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                    .thenReturn(testThread);

            try (MockedStatic userDAO = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();

                assertEquals(null, controller.CheckIdOrSlug("non-real"), "Trying to get by not existing slug");
            }
            assertEquals(testThread, ThreadDAO.getThreadBySlug("slug"));
        }
    }


    @org.junit.jupiter.api.Test
    @org.junit.jupiter.api.DisplayName("Passing not existing slug")
    void PostsTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(1))
                    .thenReturn(testThread);

            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                    .thenReturn(testThread);

            try (MockedStatic userDAO = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();

                assertThrows(NullPointerException.class, () -> controller.Posts("non-real", 10, 1, "sort", true));
            }
            assertEquals(testThread, ThreadDAO.getThreadBySlug("slug"));
        }
    }

    @org.junit.jupiter.api.Test
    @org.junit.jupiter.api.DisplayName("Passing not existing slug")
    void changeTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(1))
                    .thenReturn(testThread);

            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                    .thenReturn(testThread);

            try (MockedStatic userDAO = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();

                assertThrows(NullPointerException.class, () -> controller.change("not-existing-slug", testThread));
            }
            assertEquals(testThread, ThreadDAO.getThreadBySlug("slug"));
        }
    }

    @org.junit.jupiter.api.Test
    @org.junit.jupiter.api.DisplayName("Passing not existing slug")
    void infoTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(1))
                    .thenReturn(testThread);

            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                    .thenReturn(testThread);

            try (MockedStatic userDAO = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(null), controller.info("not existing slug"), "Trying to get by not existing slug");
            }
            assertEquals(testThread, ThreadDAO.getThreadBySlug("slug"));
        }
    }

    @org.junit.jupiter.api.Test
    @org.junit.jupiter.api.DisplayName("Passing an object of class vote, which is actually null")
    void createVoteTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(1))
                    .thenReturn(testThread);

            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                    .thenReturn(testThread);

            try (MockedStatic userDAO = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();

                assertThrows(NullPointerException.class, () -> controller.createVote("slug", testVote));
                }
            assertEquals(testThread, ThreadDAO.getThreadBySlug("slug"));
        }
    }
}